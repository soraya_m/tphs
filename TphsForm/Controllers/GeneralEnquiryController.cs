﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using TphsForm.Models;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace TphsForm.Controllers
{
    public class GeneralEnquiryController : SurfaceController
    {
        [HttpPost]
        public ActionResult HandleContactSubmit(GeneralEnquiryModel model)
        {
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("", "Captcha answer cannot be empty.");
                //return View(model);
            }

            RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                ModelState.AddModelError("", "Incorrect captcha answer.");
            }

            //model not valid, do not save, but return current umbraco page
            if (ModelState.IsValid == false)
            {
                return CurrentUmbracoPage();
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString()))
            {
                System.IO.StreamReader streamReader = null;
                try
                {                    
                    SqlCommand cmd = new SqlCommand();
                    string query = "INSERT INTO TPHS_contactus(name,email,phone,message) VALUES(@name,@email,@phone,@message)";
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@name", model.Name.Trim());
                    cmd.Parameters.AddWithValue("@email", model.Email.Trim());

                    if (!String.IsNullOrEmpty(model.Phone)) 
                        cmd.Parameters.AddWithValue("@phone", model.Phone.Trim());
                    else
                        cmd.Parameters.AddWithValue("@phone", " ");

                    if (!String.IsNullOrEmpty(model.Message)) 
                        cmd.Parameters.AddWithValue("@message", model.Message.Trim());
                    else
                        cmd.Parameters.AddWithValue("@message", " ");
                     
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    
                    string ContactUsTemplate = ConfigurationManager.AppSettings["contactUsTemplate"];
                    streamReader = new System.IO.StreamReader(Server.MapPath(ContactUsTemplate));
                    string text = streamReader.ReadToEnd();

                    text = text.Replace("$formtitle$", "<b>TPHS Contact us</b><br /><br />");
                    text = text.Replace("$title$", "<b>" + model.Title + "</b><br />");
                    text = text.Replace("$name$", "<b>" + model.Name + "</b><br />");
                    text = text.Replace("$email$", "<b>" + model.Email + "</b><br />");

                    if (!String.IsNullOrEmpty(model.Phone)) 
                        text = text.Replace("$phone$", "<b>" + model.Phone + "</b><br />");
                    else
                        text = text.Replace("$phone$", " " + "<br />");

                    if (!String.IsNullOrEmpty(model.Message)) 
                        text = text.Replace("$comments$", "<b>" + model.Message + "</b><br />");
                    else
                        text = text.Replace("$comments$", " " + "<br />");

                    string emailSubject = ConfigurationManager.AppSettings["contactUsSubject"];
                    string emailTo = ConfigurationManager.AppSettings["contactUsAdmin"];
                    string emailFrom = model.Email.Trim();
                    string sBody = "";
                    string emailConfSubject = "Pittwater House - Confirmation Email";

                    sBody = sBody + "Dear " + model.Name.Trim() + "," + System.Environment.NewLine + System.Environment.NewLine;
                    sBody = sBody + "Thank you for your enquiry to Pittwater House. Someone will be in touch with you shortly." + System.Environment.NewLine + System.Environment.NewLine;
                    sBody = sBody + "School Reception" + System.Environment.NewLine;
                    sBody = sBody + "Open 8am - 4pm school days" + System.Environment.NewLine;
                    sBody = sBody + "Tel. +61 2 9981 4400" + System.Environment.NewLine;
                    sBody = sBody + "Fax. +61 2 9971 1627" + System.Environment.NewLine;
                    sBody = sBody + "Email: school.admin@tphs.nsw.edu.au" + System.Environment.NewLine;

                    umbraco.library.SendMail(emailFrom, emailTo, emailSubject, text, true);
                    umbraco.library.SendMail(emailFrom, "boodigitalcontact@gmail.com", emailSubject, text, true);
                    umbraco.library.SendMail(emailFrom, "soraya@boodesign.com.au", emailSubject, text, true);
                    umbraco.library.SendMail(emailFrom, "pittwater.house@gmail.com", emailSubject, text, true);

                    // send the confirmation email
                    umbraco.library.SendMail(emailTo, emailFrom, emailConfSubject, sBody, false);
                    umbraco.library.SendMail(emailTo, "pittwater.house@gmail.com", emailConfSubject, sBody, false);

                    //lblMessage.Text = "Thank you for contacting Pittwater House. A confirmation of your enquiry has been sent to your email address and someone will be in touch with you shortly.";
                    
                    TempData.Add("Success", true);
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    //return RedirectToUmbracoPage(1216); // <- My published error page.
                    throw new Exception("Error : " + ex.ToString());
                }
                finally
                {
                    streamReader.Close();
                }
            }
        }

    }
}
