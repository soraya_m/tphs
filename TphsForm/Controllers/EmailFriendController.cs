﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using TphsForm.Models;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace TphsForm.Controllers
{
    public class EmailFriendController : SurfaceController
    {
        [HttpPost]
        public ActionResult HandleContactSubmit(EmailFriendModel model)
        {
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("", "Captcha answer cannot be empty.");
                //return View(model);
            }

            RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                ModelState.AddModelError("", "Incorrect captcha answer.");
            }

            //model not valid, do not save, but return current umbraco page
            if (ModelState.IsValid == false)
            {
                return CurrentUmbracoPage();
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString()))
            {
                System.IO.StreamReader streamReader = null;
                try
                {
                    string EmailTemplate = ConfigurationManager.AppSettings["tellafriendTemplate"];
                    streamReader = new System.IO.StreamReader(Server.MapPath(EmailTemplate));
                    string text = streamReader.ReadToEnd();

                    string queryString = Request.QueryString["url"];

                    text = text.Replace("$comments$", model.Message);
                    text = text.Replace("$url$", queryString);

                    string emailSubject = model.Subject.Trim();
                    string emailTo = model.EmailTo.Trim();
                    string emailFrom = model.EmailFrom.Trim();

                    umbraco.library.SendMail(emailFrom, emailTo, emailSubject, text, false);

                    TempData.Add("Success", true);
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.ToString());
                }
                finally
                {
                    streamReader.Close();
                }
            }
        }

    }
}
