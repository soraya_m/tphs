﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using TphsForm.Models;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace TphsForm.Controllers
{
    public class EnrolmentEnquiryController : SurfaceController
    {
        //public ActionResult Index()
        //{
            //return View(new EnrolmentEnquiryModel
            //{
                //Prospectus = true
                //Tour = true,
                //Australian = true,
                //OverseasStudent = true,
                //Gender = true,
                //Gender2 = true,
                //Gender3 = true,
                //Gender4 = true,
                //Gender5 = true
            //});
        //}

        [HttpPost]
        public ActionResult HandleContactSubmit(EnrolmentEnquiryModel model)
        {
            RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (String.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("", "Captcha answer cannot be empty.");
                //return View(model);
            }

            RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

            if (recaptchaResult != RecaptchaVerificationResult.Success)
            {
                ModelState.AddModelError("", "Incorrect captcha answer.");
            }

            //model not valid, do not save, but return current umbraco page
            if (ModelState.IsValid == false)
            {
                return CurrentUmbracoPage();
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ToString()))
            {
                System.IO.StreamReader streamReader = null;
                try
                {
                    SqlCommand cmd = new SqlCommand();

                    string query = "INSERT INTO TPHS_enrolment(getinfo,gettour,title,name,address1,address2,suburb,state,postcode,country,australian_resident,overseas_student,phone,mobile,email,proposed_student,gender,entry_year,entry_grade,numberofsiblings,proposed_student2,gender2,entry_year2,entry_grade2,proposed_student3,gender3,entry_year3,entry_grade3,proposed_student4,gender4,entry_year4,entry_grade4,proposed_student5,gender5,entry_year5,entry_grade5,hear_about,comments) VALUES(@getinfo,@gettour,@title,@name,@address1,@address2,@suburb,@state,@postcode,@country,@australian_resident,@overseas_student,@phone,@mobile,@email,@proposed_student,@gender,@entry_year,@entry_grade,@numberofsiblings,@proposed_student2,@gender2,@entry_year2,@entry_grade2,@proposed_student3,@gender3,@entry_year3,@entry_grade3,@proposed_student4,@gender4,@entry_year4,@entry_grade4,@proposed_student5,@gender5,@entry_year5,@entry_grade5,@hear_about,@comments)";
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;

                    if (model.Prospectus == true)
                        cmd.Parameters.AddWithValue("@getinfo", "yes");
                    else
                        cmd.Parameters.AddWithValue("@getinfo", "no");

                    if (model.Tour == true)
                        cmd.Parameters.AddWithValue("@gettour", "yes");
                    else
                        cmd.Parameters.AddWithValue("@gettour", "no");

                    cmd.Parameters.AddWithValue("@title", model.Title.Trim());
                    cmd.Parameters.AddWithValue("@name", model.Name.Trim());
                    cmd.Parameters.AddWithValue("@address1", model.Address1.Trim());
                    if (!String.IsNullOrEmpty(model.Address2)) 
                        cmd.Parameters.AddWithValue("@address2", model.Address2.Trim());
                    else
                        cmd.Parameters.AddWithValue("@address2", "");
                    cmd.Parameters.AddWithValue("@suburb", model.Suburb.Trim());
                    cmd.Parameters.AddWithValue("@state", model.State.Trim());
                    cmd.Parameters.AddWithValue("@postcode", model.Postcode.Trim());
                    cmd.Parameters.AddWithValue("@country", model.Country.Trim());

                    
                    if (model.Australian == false) // if the answer is no
                    {
                        cmd.Parameters.AddWithValue("@australian_resident", "no");

                        if (model.OverseasStudent == true)
                            cmd.Parameters.AddWithValue("@overseas_student", "yes");
                        else
                            cmd.Parameters.AddWithValue("@overseas_student", "no");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@australian_resident", "yes");
                        cmd.Parameters.AddWithValue("@overseas_student", "");
                    }

                    cmd.Parameters.AddWithValue("@phone", model.Phone.Trim());
                    if (!String.IsNullOrEmpty(model.Mobile)) 
                        cmd.Parameters.AddWithValue("@mobile", model.Mobile.Trim());
                    else
                        cmd.Parameters.AddWithValue("@mobile", "");
                    cmd.Parameters.AddWithValue("@email", model.Email.Trim());

                    if (!String.IsNullOrEmpty(model.ProposedStudent))
                        cmd.Parameters.AddWithValue("@proposed_student", model.ProposedStudent.Trim());
                    else
                        cmd.Parameters.AddWithValue("@proposed_student", "");
                    if (model.Gender == true)
                        cmd.Parameters.AddWithValue("@gender", "Male");
                    else
                        cmd.Parameters.AddWithValue("@gender", "Female");
                    cmd.Parameters.AddWithValue("@entry_year", model.ProposedYear.Trim());
                    cmd.Parameters.AddWithValue("@entry_grade", model.ProposedGrade.Trim());

                    cmd.Parameters.AddWithValue("@numberofsiblings", model.Sibling);

                    if (!String.IsNullOrEmpty(model.ProposedStudent2))
                        cmd.Parameters.AddWithValue("@proposed_student2", model.ProposedStudent2.Trim());
                    else
                        cmd.Parameters.AddWithValue("@proposed_student2", "");
                    if (model.Gender2 == true)
                        cmd.Parameters.AddWithValue("@gender2", "Male");
                    else
                        cmd.Parameters.AddWithValue("@gender2", "Female");
                    cmd.Parameters.AddWithValue("@entry_year2", model.ProposedYear2.Trim());
                    cmd.Parameters.AddWithValue("@entry_grade2", model.ProposedGrade2.Trim());

                    if (!String.IsNullOrEmpty(model.ProposedStudent3))
                        cmd.Parameters.AddWithValue("@proposed_student3", model.ProposedStudent3.Trim());
                    else
                        cmd.Parameters.AddWithValue("@proposed_student3", "");
                    if (model.Gender3 == true)
                        cmd.Parameters.AddWithValue("@gender3", "Male");
                    else
                        cmd.Parameters.AddWithValue("@gender3", "Female");
                    cmd.Parameters.AddWithValue("@entry_year3", model.ProposedYear3.Trim());
                    cmd.Parameters.AddWithValue("@entry_grade3", model.ProposedGrade3.Trim());

                    if (!String.IsNullOrEmpty(model.ProposedStudent4))
                        cmd.Parameters.AddWithValue("@proposed_student4", model.ProposedStudent4.Trim());
                    else
                        cmd.Parameters.AddWithValue("@proposed_student4", "");
                    if (model.Gender4 == true)
                        cmd.Parameters.AddWithValue("@gender4", "Male");
                    else
                        cmd.Parameters.AddWithValue("@gender4", "Female");
                    cmd.Parameters.AddWithValue("@entry_year4", model.ProposedYear4.Trim());
                    cmd.Parameters.AddWithValue("@entry_grade4", model.ProposedGrade4.Trim());

                    if (!String.IsNullOrEmpty(model.ProposedStudent5))
                        cmd.Parameters.AddWithValue("@proposed_student5", model.ProposedStudent5.Trim());
                    else
                        cmd.Parameters.AddWithValue("@proposed_student5", "");
                    if (model.Gender5 == true)
                        cmd.Parameters.AddWithValue("@gender5", "Male");
                    else
                        cmd.Parameters.AddWithValue("@gender5", "Female");
                    cmd.Parameters.AddWithValue("@entry_year5", model.ProposedYear5.Trim());
                    cmd.Parameters.AddWithValue("@entry_grade5", model.ProposedGrade5.Trim());

                    if (model.HearAbout.Trim() == "Other")
                        if (!String.IsNullOrEmpty(model.HearOther))
                            cmd.Parameters.AddWithValue("@hear_about", model.HearAbout.Trim() + " - " + model.HearOther.Trim());
                        else
                            cmd.Parameters.AddWithValue("@hear_about", model.HearAbout.Trim());
                    else
                        cmd.Parameters.AddWithValue("@hear_about", model.HearAbout.Trim());

                    if (!String.IsNullOrEmpty(model.Comments))
                        cmd.Parameters.AddWithValue("@comments", model.Comments.Trim());
                    else
                        cmd.Parameters.AddWithValue("@comments", "");
                    
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    
                    string EnrolmentTemplate = ConfigurationManager.AppSettings["enrolmentTemplate"];
                    streamReader = new System.IO.StreamReader(Server.MapPath(EnrolmentTemplate));
                    string text = streamReader.ReadToEnd();

                    text = text.Replace("$formtitle$", "<b>TPHS Enrolment</b><br /><br />");

                    if (model.Prospectus == true)
                        text = text.Replace("$getinfo$", "<b>yes</b><br />");
                    else
                        text = text.Replace("$getinfo$", "<b>no</b><br />");

                    if (model.Tour == true)
                        text = text.Replace("$booktour$", "<b>yes</b><br /><br />");
                    else
                        text = text.Replace("$booktour$", "<b>no</b><br /><br />");

                    text = text.Replace("$title$", "<b>" + model.Title.Trim() + "</b><br />");
                    text = text.Replace("$fullname$", "<b>" + model.Name.Trim() + "</b><br />");
                    text = text.Replace("$address1$", "<b>" + model.Address1.Trim() + "</b><br />");
                    if (!String.IsNullOrEmpty(model.Address2))
                        text = text.Replace("$address2$", "<b>" + model.Address2.Trim() + "</b><br />");
                    else
                        text = text.Replace("$address2$", " " + "<br />");
                    text = text.Replace("$suburb$", "<b>" + model.Suburb.Trim() + "</b><br />");
                    text = text.Replace("$state$", "<b>" + model.State.Trim() + "</b><br />");
                    text = text.Replace("$postcode$", "<b>" + model.Postcode.Trim() + "</b><br />");
                    text = text.Replace("$country$", "<b>" + model.Country.Trim() + "</b><br />");

                    if (model.Australian == false) // if the answer is no
                    {
                        text = text.Replace("$resident$", "<b>No</b><br />");
                        if (model.OverseasStudent == true)
                            text = text.Replace("$overseas$", "Does this enquiry relate to an overseas student? : " + "<b>Yes</b><br />");
                        else
                            text = text.Replace("$overseas$", "Does this enquiry relate to an overseas student? : No" + "<br />");
                    }
                    else
                    {
                        text = text.Replace("$resident$", "Yes" + "<br />");
                        text = text.Replace("$overseas$", "" + "<br />");
                    }

                    text = text.Replace("$homephone$", "<b>" + model.Phone.Trim() + "</b><br />");
                    if (!String.IsNullOrEmpty(model.Mobile))
                        text = text.Replace("$mobilephone$", "<b>" + model.Mobile.Trim() + "</b><br />");
                    else
                        text = text.Replace("$mobilephone$", " " + "<br />");
                    text = text.Replace("$email$", "<b>" + model.Email.Trim() + "</b><br /><br />");

                    if (!String.IsNullOrEmpty(model.ProposedStudent))
                        text = text.Replace("$nameapplicant$", "<b>" + model.ProposedStudent.Trim() + "</b><br />");
                    else
                        text = text.Replace("$nameapplicant$", " " + "<br />");
                    if (model.Gender == true)
                        text = text.Replace("$gender$", "<b>Male</b><br />");
                    else
                        text = text.Replace("$gender$", "<b>Female</b><br />");
                    
                    text = text.Replace("$yearentry$", "<b>" + model.ProposedYear.Trim() + "</b><br />");
                    text = text.Replace("$gradeentry$", "<b>" + model.ProposedGrade.Trim() + "</b><br /><br />");

                    text = text.Replace("$siblings$", "<b>" + model.Sibling.Trim() + "</b><br />");

                    string sData = "";
                    if (model.Sibling.Trim() == "1")
                    {
                        if (!String.IsNullOrEmpty(model.ProposedStudent2))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent2.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";

                        if (model.Gender2 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear2.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade2.Trim() + "</b><br />";
                    }
                    else if (model.Sibling.Trim() == "2")
                    {
                        if (!String.IsNullOrEmpty(model.ProposedStudent2))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent2.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";

                        if (model.Gender2 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear2.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade2.Trim() + "</b><br /><br />";

                        if (!String.IsNullOrEmpty(model.ProposedStudent3))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent3.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";
                        if (model.Gender3 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear3.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade3.Trim() + "</b><br />";
                    }

                    else if (model.Sibling.Trim() == "3")
                    {
                        if (!String.IsNullOrEmpty(model.ProposedStudent2))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent2.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";

                        if (model.Gender2 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear2.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade2.Trim() + "</b><br /><br />";

                        if (!String.IsNullOrEmpty(model.ProposedStudent3))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent3.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";

                        if (model.Gender3 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear3.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade3.Trim() + "</b><br /><br />";

                        if (!String.IsNullOrEmpty(model.ProposedStudent4))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent4.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";
                        if (model.Gender4 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear4.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade4.Trim() + "</b><br />";
                    }

                    else if (model.Sibling.Trim() == "4")
                    {
                        if (!String.IsNullOrEmpty(model.ProposedStudent2))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent2.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";

                        if (model.Gender2 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear2.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade2.Trim() + "</b><br /><br />";

                        if (!String.IsNullOrEmpty(model.ProposedStudent3))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent3.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";
                        if (model.Gender3 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear3.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade3.Trim() + "</b><br /><br />";

                        if (!String.IsNullOrEmpty(model.ProposedStudent4))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent4.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";
                        if (model.Gender4 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear4.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade4.Trim() + "</b><br /><br />";

                        if (!String.IsNullOrEmpty(model.ProposedStudent5))
                            sData = sData + "Name of proposed student: " + "<b>" + model.ProposedStudent5.Trim() + "</b><br />";
                        else
                            sData = sData + "Name of proposed student: " + " " + "<br />";
                        if (model.Gender5 == true)
                            sData = sData + "Gender: " + "<b>Male</b><br />";
                        else
                            sData = sData + "Gender: " + "<b>Female</b><br />";
                        sData = sData + "Proposed year of entry: " + "<b>" + model.ProposedYear5.Trim() + "</b><br />";
                        sData = sData + "Proposed grade of entry: " + "<b>" + model.ProposedGrade5.Trim() + "</b><br />";
                    }

                    text = text.Replace("$othersiblings$", sData + "<br />");

                    if (model.HearAbout.Trim() == "Other")
                    {
                        text = text.Replace("$hearabout$", "<b>" + model.HearAbout.Trim() + "</b><br />");
                        if (!String.IsNullOrEmpty(model.HearOther))
                            text = text.Replace("$hearother$", " - " + "<b>" + model.HearOther.Trim() + "</b><br />");
                        else
                            text = text.Replace("$hearother$", " - " + "<br />");
                    }
                    else
                    {
                        text = text.Replace("$hearabout$", "<b>" + model.HearAbout.Trim() + "</b><br />");
                        text = text.Replace("$hearother$", "" + "<br />");
                    }

                    if (!String.IsNullOrEmpty(model.Comments))
                        text = text.Replace("$comments$", "<b>" + model.Comments.Trim() + "</b><br /><br />");
                    else
                        text = text.Replace("$comments$", " " + "<br /><br />");

                    if (model.Agree == true)
                        text = text.Replace("$tphscontact$", "<b>Yes, I would like Pittwater House to contact me with marketing material, news and events that may be of interest to me</b><br />");
                    else
                        text = text.Replace("$tphscontact$", " " + "<br />");

                    string emailSubject = ConfigurationManager.AppSettings["enrolmentSubject"];
                    string emailTo = ConfigurationManager.AppSettings["enrolmentAdmin"];
                    //string emailTo = "soraya@boodesign.com.au";
                    string emailFrom = model.Email.Trim();

                    string sBody = "";
                    string emailConfSubject = "Pittwater House - Confirmation Email";

                    if (model.OverseasStudent == true) // for overseas student
                    {
                        string overseasEmail = "overseas.enrolments@tphs.nsw.edu.au";
                        //string overseasEmail = "michelle.norton@tphs.nsw.edu.au";
                        //string overseasEmail = "soraya@boodesign.com.au";

                        sBody = sBody + "Dear " + model.Name.Trim() + ",<br /><br />";
                        if (model.Prospectus == true && model.Tour == true)  // prospectus and tour are selected
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House. A prospectus and information pack will be sent shortly and our Overseas Students Admissions Manager will be in touch to assist you with booking a tour of the school.<br /><br />";
                            sBody = sBody + "Please see below for our tour times.<br />";
                            sBody = sBody + "Senior School Tour – Mondays at 9:15am<br />";
                            sBody = sBody + "Campus Tour (whole school) – Tuesdays at 9:15am and Thursdays at 9:15am<br />";
                            sBody = sBody + "Early Childhood Centre and Junior School Tour – Fridays at 9:15am<br /><br />";
                            sBody = sBody + "Tours are conducted most weeks during school terms and last for approximately an hour. The tours at Pittwater House are usually led by the Heads of the Junior and Senior School or Our School Principal, Dr Nancy Hillier. This provides families with a wonderful opportunity to get a good insight into the school and ask detailed questions.<br /><br />";
                            sBody = sBody + "Please note, bookings are essential.<br /><br />";
                            sBody = sBody + "In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        else if (model.Prospectus == true && model.Tour == false)  // prospectus is selected
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House, a prospectus and information pack will be sent shortly. Our Overseas Students Admissions Manager will be in touch in the coming weeks to follow up if you require further information or assistance. In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        else if (model.Prospectus == false && model.Tour == true)  // tour is selected
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House. Our Overseas Students Admissions Manager will be in touch to assist you with booking a tour of the school.<br /><br />";
                            sBody = sBody + "Please see below for our tour times.<br />";
                            sBody = sBody + "Senior School Tour – Mondays at 9:15am<br />";
                            sBody = sBody + "Campus Tour (whole school) – Tuesdays at 9:15am and Thursdays at 9:15am<br />";
                            sBody = sBody + "Early Childhood Centre and Junior School Tour – Fridays at 9:15am<br /><br />";
                            sBody = sBody + "Tours are conducted most weeks during school terms and last for approximately an hour. The tours at Pittwater House are usually led by the Heads of the Junior and Senior School or Our School Principal, Dr Nancy Hillier. This provides families with a wonderful opportunity to get a good insight into the school and ask detailed questions.<br /><br />";
                            sBody = sBody + "Please note, bookings are essential.<br /><br />";
                            sBody = sBody + "In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        else
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House. Our Overseas Students Admissions Manager will contact you shortly to assist you with your enquiry. In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        sBody = sBody + "Overseas Students Admissions Office<br />";
                        sBody = sBody + "Open 8am - 4pm school days<br />";
                        sBody = sBody + "Email: overseas.enrolments@tphs.nsw.edu.au<br />";

                        umbraco.library.SendMail(emailFrom, overseasEmail, emailSubject, text, true);
                        //umbraco.library.SendMail(emailFrom, "max@boodigital.com.au", emailSubject, text, true);
                        umbraco.library.SendMail(emailFrom, "boodigitalcontact@gmail.com", emailSubject, text, true);
                        umbraco.library.SendMail(emailFrom, "pittwater.house@gmail.com", emailSubject, text, true);

                        // send the confirmation email
                        umbraco.library.SendMail(overseasEmail, emailFrom, emailConfSubject, sBody, true);
                        //umbraco.library.SendMail(overseasEmail, "pittwater.house@gmail.com", emailConfSubject, sBody, false);
                    }
                    else
                    {
                        sBody = sBody + "Dear " + model.Name.Trim() + ",<br /><br />";
                        if (model.Prospectus == true && model.Tour == true)  // prospectus and tour are selected
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House. A prospectus and information pack will be sent shortly and our Registrar will be in touch to assist you with booking a tour of the school.<br /><br />";
                            sBody = sBody + "Please see below for our tour times.<br />";
                            sBody = sBody + "Senior School Tour – Mondays at 9:15am<br />";
                            sBody = sBody + "Campus Tour (whole school) – Tuesdays at 9:15am and Thursdays at 9:15am<br />";
                            sBody = sBody + "Early Childhood Centre and Junior School Tour – Fridays at 9:15am<br /><br />";
                            sBody = sBody + "Tours are conducted most weeks during school terms and last for approximately an hour. The tours at Pittwater House are usually led by the Heads of the Junior and Senior School or Our School Principal, Dr Nancy Hillier. This provides families with a wonderful opportunity to get a good insight into the school and ask detailed questions.<br /><br />";
                            sBody = sBody + "Please note, bookings are essential.<br /><br />";
                            sBody = sBody + "In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        else if (model.Prospectus == true && model.Tour == false)  // prospectus is selected
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House, a prospectus and information pack will be sent shortly. Our Admissions Manager will be in touch in the coming weeks to follow up if you require further information or assistance. In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        else if (model.Prospectus == false && model.Tour == true)  // tour is selected
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House. Our Registrar will be in touch to assist you with booking a tour of the school.<br /><br />";
                            sBody = sBody + "Please see below for our tour times.<br />";
                            sBody = sBody + "Senior School Tour – Mondays at 9:15am<br />";
                            sBody = sBody + "Campus Tour (whole school) – Tuesdays at 9:15am and Thursdays at 9:15am<br />";
                            sBody = sBody + "Early Childhood Centre and Junior School Tour – Fridays at 9:15am<br /><br />";
                            sBody = sBody + "Tours are conducted most weeks during school terms and last for approximately an hour. The tours at Pittwater House are usually led by the Heads of the Junior and Senior School or Our School Principal, Dr Nancy Hillier. This provides families with a wonderful opportunity to get a good insight into the school and ask detailed questions.<br /><br />";
                            sBody = sBody + "Please note, bookings are essential.<br /><br />";
                            sBody = sBody + "In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        else
                        {
                            sBody = sBody + "Thank you for your enquiry regarding enrolment at Pittwater House. Our Admissions Manager will contact you shortly to assist you with your enquiry. In the meantime, should you have any questions please do not hesitate to contact us directly.<br /><br />";
                        }
                        sBody = sBody + "Admissions Office<br />";
                        sBody = sBody + "Open 8am - 4pm school days<br />";
                        sBody = sBody + "Tel. +61 2 9972 5789<br />";
                        sBody = sBody + "Fax. +61 2 9972 5779<br />";
                        sBody = sBody + "Email: enrolments@tphs.nsw.edu.au<br />";

                        umbraco.library.SendMail(emailFrom, emailTo, emailSubject, text, true);
                        umbraco.library.SendMail(emailFrom, "soraya@boodesign.com.au", emailSubject, text, true);
                        umbraco.library.SendMail(emailFrom, "boodigitalcontact@gmail.com", emailSubject, text, true);
                        umbraco.library.SendMail(emailFrom, "pittwater.house@gmail.com", emailSubject, text, true);

                        // send the confirmation email
                        umbraco.library.SendMail(emailTo, emailFrom, emailConfSubject, sBody, true);
                        umbraco.library.SendMail(emailTo, "pittwater.house@gmail.com", emailConfSubject, sBody, false);
                    }
                    
                    TempData.Add("Success", true);
                    return RedirectToCurrentUmbracoPage();
                }
                catch (Exception ex)
                {
                    //return RedirectToUmbracoPage(1056); // <- My published error page.
                    throw new Exception("Error : " + ex.ToString());
                }
                finally
                {
                    streamReader.Close();
                }
            }
        }

    }
}
