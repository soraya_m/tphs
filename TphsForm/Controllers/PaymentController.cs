﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using TphsForm.Models;

namespace TphsForm.Controllers
{
    public class PaymentController : SurfaceController
    {
        //
        // GET: /Payment/

        [HttpPost]
        public ActionResult HandleContactSubmit(PaymentModel model)
        {
            if (model.PaymentOption == "1")
            {
                return Redirect("https://www.bpoint.com.au/payments/pittwater.eforms/50190205573");
            }
            else if (model.PaymentOption == "2")
            {
                return Redirect("https://www.bpoint.com.au/payments/pittwater.eforms/50191619447");
            }
            else if (model.PaymentOption == "3")
            {
                return Redirect("https://www.bpoint.com.au/payments/pittwater.eforms/50192820218");
            }
            else if (model.PaymentOption == "4")
            {
                return Redirect("https://www.bpoint.com.au/payments/pittwater.eforms/50193802902");
            }
            else
            {
                return RedirectToCurrentUmbracoPage();
            }

        }

    }
}
