﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel;

namespace TphsForm.Models
{
    public class GeneralEnquiryModel
    {
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your full name")]
        [DisplayName("Full name")]
        public string Name { get; set; }

        [DisplayName("Phone Number")]
        public string Phone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your email address")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email Format")]
        public string Email { get; set; }

        public string Message { get; set; }
    }
}