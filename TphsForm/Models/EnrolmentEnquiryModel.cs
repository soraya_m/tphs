﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel;
namespace TphsForm.Models
{
    public class EnrolmentEnquiryModel
    {
        [DisplayName("I would like to receive an information pack and prospectus")]
        public bool Prospectus { get; set; }

        [DisplayName("I would like to experience the school live in action - please book me on a tour")]
        public bool Tour { get; set; }

        public string Title { get; set; }

        [DisplayName("Full name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your full name")]
        public string Name { get; set; }

        [DisplayName("Address 1")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your postal address")]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your suburb")]
        public string Suburb { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your state")]
        public string State { get; set; }

        [DisplayName("Post Code")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your post code")]
        public string Postcode { get; set; }

        public string Country { get; set; }

        [DisplayName("Are you an Australian Resident?")]
        public bool Australian { get; set; }

        [DisplayName("Does this enquiry relate to an overseas student?")]
        public bool OverseasStudent { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your phone number")]
        public string Phone { get; set; }

        [DisplayName("Mobile Phone")]
        public string Mobile { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your email")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email Format")]
        public string Email { get; set; }

        [DisplayName("Name of proposed student")]
        public string ProposedStudent { get; set; }

        public bool Gender { get; set; }

        [DisplayName("Proposed year of entry")]
        public string ProposedYear { get; set; }

        [DisplayName("Proposed grade of entry")]
        public string ProposedGrade { get; set; }

        [DisplayName("Number of siblings")]
        public string Sibling { get; set; }

        [DisplayName("1. Name of proposed student")]
        public string ProposedStudent2 { get; set; }

        public bool Gender2 { get; set; }

        [DisplayName("Proposed year of entry")]
        public string ProposedYear2 { get; set; }

        [DisplayName("Proposed grade of entry")]
        public string ProposedGrade2 { get; set; }

        [DisplayName("2. Name of proposed student")]
        public string ProposedStudent3 { get; set; }

        public bool Gender3 { get; set; }

        [DisplayName("Proposed year of entry")]
        public string ProposedYear3 { get; set; }

        [DisplayName("Proposed grade of entry")]
        public string ProposedGrade3 { get; set; }

        [DisplayName("3. Name of proposed student")]
        public string ProposedStudent4 { get; set; }

        public bool Gender4 { get; set; }

        [DisplayName("Proposed year of entry")]
        public string ProposedYear4 { get; set; }

        [DisplayName("Proposed grade of entry")]
        public string ProposedGrade4 { get; set; }

        [DisplayName("4. Name of proposed student")]
        public string ProposedStudent5 { get; set; }

        public bool Gender5 { get; set; }

        [DisplayName("Proposed year of entry")]
        public string ProposedYear5 { get; set; }

        [DisplayName("Proposed grade of entry")]
        public string ProposedGrade5 { get; set; }

        [DisplayName("How did you hear about us?")]
        public string HearAbout { get; set; }

        public string HearOther { get; set; }

        public string Comments { get; set; }

        [DisplayName("Yes, I would like Pittwater House to contact me with marketing material, news and events that may be of interest to me")]
        public bool Agree { get; set; }
    }
}